# node-web-app

Hello WIlliam Hill!! :D
Image available at: https://hub.docker.com/repository/docker/renatofenrir/node-web-app

To run the application as a stand-alone container:

docker run -p 49160:8080 -d renatofenrir/node-web-app:latest

To run the application on a k8s cluster, do:

kubectl create -f k8s/

Then check if the deployment is created, deploying 3 replicas:

kubectl get pods --all-namespaces |grep node-web-app


Also, check if the service is already available, exposing the correct ports:

kubectl get svc |grep node-web-app


If everything is ok, then test the endpoint by doing:

localhost:32210
